module mep-agent

go 1.14

require (
	github.com/BurntSushi/toml v0.3.1 // indirect
	github.com/agiledragon/gomonkey v2.0.1+incompatible
	github.com/go-playground/validator/v10 v10.2.0
	github.com/natefinch/lumberjack v2.0.0+incompatible
	github.com/sirupsen/logrus v1.4.2
	github.com/smartystreets/goconvey v1.6.4
	gopkg.in/natefinch/lumberjack.v2 v2.0.0 // indirect
	gopkg.in/yaml.v2 v2.2.8
)
